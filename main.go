package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/ahanselka/go-gitlab"
)

var apiKey = flag.String("api-key", "", "api key!")
var timeZone = flag.String("timezone", "UTC", "Timezone")

func getLocalHour(timezone string) int {
	loc, _ := time.LoadLocation(timezone)
	t := time.Now().In(loc)
	hour := t.Hour()
	return hour
}

func getEmojiName(hour int) string {
	emojiHour := ""
	if hour == 0 {
		emojiHour = strconv.Itoa(12)
	} else if hour > 12 {
		emojiHour = strconv.Itoa(hour - 12)
	} else {
		emojiHour = strconv.Itoa(hour)
	}
	emojiName := fmt.Sprintf("clock%v", emojiHour)
	return emojiName
}

func generateMessage(hour int) string {
	return fmt.Sprintf("The time is now %d:00", hour)
}
func main() {
	flag.Parse()
	git := gitlab.NewClient(nil, *apiKey)
	hour := getLocalHour(*timeZone)
	emojiName := getEmojiName(hour)
	message := generateMessage(hour)

	opts := &gitlab.UserStatusOptions{
		Emoji:   gitlab.String(emojiName),
		Message: gitlab.String(message),
	}
	_, _, err := git.Users.SetUserStatus(opts)
	if err != nil {
		log.Fatal(err)
	}
}
